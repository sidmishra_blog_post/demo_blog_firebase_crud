
  const firebaseConfig = {
    apiKey: "AIzaSyAvp6lQxVDWPsXq5ai8yTrp3tLH8cqw9h0",
    authDomain: "fir-blog-firebase-crud.firebaseapp.com",
    databaseURL: "https://fir-blog-firebase-crud-default-rtdb.firebaseio.com",
    projectId: "fir-blog-firebase-crud",
    storageBucket: "fir-blog-firebase-crud.appspot.com",
    messagingSenderId: "837078363856",
    appId: "1:837078363856:web:d3deee3de6357a00e7f8d5"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Set database variable
var database = firebase.database()

function save() {
  var email = document.getElementById('email').value
  var password = document.getElementById('password').value
  var username = document.getElementById('username').value
  var say_something = document.getElementById('say_something').value
  var favourite_food = document.getElementById('favourite_food').value

  database.ref('users/' + username).set({
    email : email,
    password : password,
    username : username,
    say_something : say_something,
    favourite_food : favourite_food
  })

  alert('Saved')
}

function get() {
  var username = document.getElementById('username').value

  var user_ref = database.ref('users/' + username)
  user_ref.on('value', function(snapshot) {
    var data = snapshot.val()

    alert(data.email)

  })

}

function update() {
  var username = document.getElementById('username').value
  var email = document.getElementById('email').value
  var password = document.getElementById('password').value

  var updates = {
    email : email,
    password : password
  }

  database.ref('users/' + username).update(updates)

  alert('updated')
}

function remove() {
  var username = document.getElementById('username').value

  database.ref('users/' + username).remove()

  alert('deleted')
}

